module.exports = {
	addUser: async (userEvent, data) => {
		const eventData = new userEvent(data);
		const queryResult = await eventData.save();
		return {
			id: queryResult.id,
			email: queryResult.email
		};
	},

	findUser: async (userEvent, email) => {
		const eventData = { email };
		const queryResult = await userEvent.findOne(eventData);
		return queryResult;
	}
}
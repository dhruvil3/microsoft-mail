require('dotenv').config();

const express = require('express');

const confidentialClientApplication = require('./utils/clientApplication');
const generateGraphClient = require('./utils/graph');

const userEvent = require('./utils/mongooseConfig').models.userModel;
const { codeRequest, tokenRequest } = require('./config');
const { getAccountFromCacheByUsername } = require('./utils/helper');
const service = require('./service');

const app = express();
const port = process.env.PORT;

app.use(express.json());

app.get('/', async (req, res) => {
	try {
		const authCodeUrl = await confidentialClientApplication.getAuthCodeUrl(codeRequest);
		res.redirect(authCodeUrl);
	} catch (error) {
		console.log(error);
		res.status(500).send(error);
	}
});

app.get('/redirect', async (req, res) => {
	try {
		tokenRequest.code = req.query.code;
		const tokenData = await confidentialClientApplication.acquireTokenByCode(tokenRequest);
		const msalTokenCache = confidentialClientApplication.getTokenCache();
		const account = await getAccountFromCacheByUsername(msalTokenCache, tokenData.account.username);
		const data = {
			email: tokenData.account.username,
			accessToken: tokenData.accessToken,
			account
		}
		const refreshToken = await confidentialClientApplication.getTokenCache().storage;
		console.log(refreshToken);
		const response = await service.addUser(userEvent, data);
		res.status(200).send(response);
	} catch (error) {
		console.log(error);
		res.status(500).send(error);
	}
});

app.get('/getAccessToken', async (req, res) => {
	try {
		const { username } = req.query;
		const userData = await service.findUser(userEvent, username);
		console.log(userData);
		const { account } = userData;
		const accessTokenRequest = {
			scopes: ["mail.send"],
			account
		};
		const response = await confidentialClientApplication.acquireTokenSilent(
			accessTokenRequest,
			{
				forceRefresh: false
			}
		);
		res.status(200).send(response);
	} catch (error) {
		console.log(error);
		res.status(500).send(error);
	}
});

app.post('/sendMail', async (req, res) => {
	try {
		const { username } = req.query;
		const userData = await service.findUser(userEvent, username);
		const { accessToken } = userData;
		const client = generateGraphClient(accessToken);
		const mailBody = {
			message: req.body
		};
		const response = await client.api('/me/sendMail').post(mailBody);
		res.status(200).send(response);
	} catch (error) {
		console.log(error);
		res.status(500).send(error);
	}
});

app.listen(port, () => {
	console.log(`Server is running on port ${port}`);
});

const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  email: { type: String, required: true, unique: true },
  accessToken: { type: String, require: true },
  account: { type: Object, require: true }
})

const userModel = mongoose.model('user', UserSchema);

const makeConnection = () => {
  mongoose.connect(process.env.DATABASE_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
  }, (error) => {
    if (error) throw new Error('Connection failed with mongodb');
  });
  console.log('Connected');
}

makeConnection();

module.exports = {
  mongoose,
  models: {
    userModel
  },
};
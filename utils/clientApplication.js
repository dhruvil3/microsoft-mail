const msal = require('@azure/msal-node');
const { clientAuthConfig } = require('../config');

const clientConfig = {
	auth: clientAuthConfig,
};

const confidentialClientApplication = new msal.ConfidentialClientApplication(clientConfig);

module.exports = confidentialClientApplication;

// *** REFRESH TOKEN NOT SENT IN RESPONSE ***
// https://github.com/AzureAD/microsoft-authentication-library-for-js/issues/2836

// *** GET NEW ACCESS TOKEN ***
// https://github.com/AzureAD/microsoft-authentication-library-for-js/issues/2155

// *** AUTHORITY ***
// https://docs.microsoft.com/en-us/azure/active-directory/develop/tutorial-v2-javascript-spa

// *** RATE LIMIT **
// https://docs.microsoft.com/en-us/graph/throttling

// *** TUTORIAL ***
// https://docs.microsoft.com/en-us/graph/tutorials/node?tutorial-step=3

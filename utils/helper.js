module.exports = {
	getAccountFromCacheByUsername: async (cache, username) => {
		const cachedAccounts = await cache.getAllAccounts();
		for (account of cachedAccounts) {
			if (account.username === username) return account;
		}
	}
};
const graph = require('@microsoft/microsoft-graph-client');
require('isomorphic-fetch');

const generateGraphClient = (accessToken) => {
	return graph.Client.init({
		authProvider: (done) => {
			done(null, accessToken);
		}
	});
}

module.exports = generateGraphClient;
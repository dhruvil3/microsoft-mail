require('dotenv').config();

module.exports = {
	clientAuthConfig: {
		clientId: process.env.OAUTH_APP_ID,
		authority: process.env.OAUTH_AUTHORITY,
		clientSecret: process.env.OAUTH_APP_SECRET
	},

	codeRequest: {
		scopes: ['user.read', 'mail.send'],
		redirectUri: process.env.OAUTH_REDIRECT_URI
	},

	tokenRequest: {
		scopes: ['user.read', 'mail.send'],
		redirectUri: process.env.OAUTH_REDIRECT_URI
	}
}

// sendMailFormat: {
// 	subject: "Microsoft Graph JavaScript Sample",
// 	toRecipients: [
// 		{
// 			emailAddress: {
// 				address: "dhruvil@zuru.tech",
// 			},
// 		},
// 	],
// 	body: {
// 		content: "<h1>MicrosoftGraph JavaScript Sample</h1>Check out https://github.com/microsoftgraph/msgraph-sdk-javascript",
// 		contentType: "html",
// 	}
// }